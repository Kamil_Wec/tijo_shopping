package pl.edu.pwsztar;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ProductPriceTest {

    @Test
    void getProductPrice(){
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Milk",4,2);
        shoppingCart.addProducts("Water",2,12);
        shoppingCart.addProducts("Eggs",8,1);

        assertEquals(4,shoppingCart.getProductPrice("Milk"));
        assertEquals(2,shoppingCart.getProductPrice("Water"));
        assertEquals(8,shoppingCart.getProductPrice("Eggs"));
        assertEquals(0,shoppingCart.getProductPrice("Vodka"));
    }
}
