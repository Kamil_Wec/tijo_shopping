package pl.edu.pwsztar;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DeleteProductTest {

    @Test
    void deleteProductTest(){
        ShoppingCart shoppingCart = new ShoppingCart();

        Product mock1 = new Product("Milk",2,3);
        Product mock2 = new Product("Water",1,3);
        Product mock3 = new Product("Vodka",32,1);
        shoppingCart.addProducts(mock1.getName(),mock1.getPrice(),mock1.getQuantity());
        shoppingCart.addProducts(mock2.getName(),mock2.getPrice(),mock2.getQuantity());
        shoppingCart.addProducts(mock3.getName(),mock3.getPrice(),mock3.getQuantity());

        assertTrue(shoppingCart.deleteProducts(mock1.getName(), 2));
        assertTrue(shoppingCart.deleteProducts(mock1.getName(), 1));
        assertFalse(shoppingCart.deleteProducts(mock1.getName(), 1));
        assertFalse(shoppingCart.deleteProducts(mock2.getName(), 4));
        assertFalse(shoppingCart.deleteProducts(mock3.getName(), 4));
        assertFalse(shoppingCart.deleteProducts("Whiskey", 4));
    }
}
