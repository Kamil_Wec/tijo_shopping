package pl.edu.pwsztar;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


public class SumPriceTest {

    @Test
    void sumPriceOfProducts() {
        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart.addProducts("Bread",3,3);
        shoppingCart.addProducts("Cigarettes",18,1);
        shoppingCart.addProducts("Beer",3,6);
        shoppingCart.addProducts("Cola",5,2);
        shoppingCart.addProducts("Butter",5,1);

        assertEquals(60,shoppingCart.getSumProductsPrices());

        shoppingCart.deleteProducts("Cola",1);
        assertEquals(55,shoppingCart.getSumProductsPrices());

        shoppingCart.deleteProducts("Bread",4);
        assertEquals(55,shoppingCart.getSumProductsPrices());

    }
}
